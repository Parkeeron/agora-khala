package org.khala.resource

import com.andreapivetta.kolor.blue
import com.andreapivetta.kolor.cyan
import com.andreapivetta.kolor.green
import com.andreapivetta.kolor.yellow
import org.jetbrains.exposed.exceptions.ExposedSQLException
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SchemaUtils.create
import org.jetbrains.exposed.sql.statements.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import org.khala.getGlobalSettings
import org.khala.makeServer
import java.sql.PreparedStatement
import java.sql.SQLException

object ServerTable: Table("servers") {
  val id: Column<Int> = integer("id").autoIncrement().primaryKey()
  val name: Column<String> = varchar("name", 36)
  val priIp: Column<String> = varchar("pri_ip", 15)
  val pubIp: Column<String> = varchar("pub_ip", 15)
  val detailIp: Column<String> =	varchar("detail_ip", 128)
  val idc: Column<String> =	varchar("idc", 30)
  val provider: Column<String> = varchar("provider", 20)
  val voserver: Column<Boolean> = bool("voserver")
  val access_point: Column<Boolean>	= bool("access_point")
  val vorouter: Column<Boolean>	= bool("vorouter")
  val votest: Column<Boolean>	= bool("votest")
  val vobackbone: Column<Boolean>	= bool("vobackbone")
  val vom: Column<Boolean>	= bool("vom")
  val voc: Column<Boolean>	= bool("voc")
  val voqa: Column<Boolean>	= bool("voqa")
  val vocs: Column<Boolean>	= bool("vocs")
  val voo: Column<Boolean>	= bool("voo")
  val balancer2: Column<Boolean> = bool("balancer2")
  val balancer_proxy: Column<Boolean> = bool("balancer_proxy")
  val config_server: Column<Boolean> = bool("config_server")
  val auth_server: Column<Boolean> = bool("auth_server")
  val rule_server: Column<Boolean> = bool("rule_server")
  val xcode: Column<Boolean> = bool("xcode")
  val r_edge: Column<Boolean> = bool("r_edge")
  val r_relay: Column<Boolean> = bool("r_relay")
  val lbs: Column<Boolean> = bool("lbs")
  val webrtc_gateway: Column<Boolean> = bool("webrtc_gateway")
  val choose_server: Column<Boolean> = bool("choose_server")
  val r_eslb: Column<Boolean> = bool("r_eslb")
  val video_publisher: Column<Boolean> = bool("video_publisher")
  val stun: Column<Boolean> = bool("stun")
  val cops: Column<Boolean> = bool("cops")
  val uni_lbs: Column<Int> = integer("uni_lbs")
  val registrar: Column<Boolean> = bool("registrar")
  val forwarder: Column<Boolean> = bool("forwarder")
  val edge: Column<Boolean> = bool("edge")
  val relay: Column<Boolean> = bool("relay")
  val smokeping: Column<Boolean> = bool("smokeping")
  val other: Column<String> = varchar("other", 20)
  val up_time: Column<DateTime> = datetime("up_time")
  val down_time: Column<DateTime> = datetime("down_time")
  val waiting: Column<Int> = integer("waiting")
  val sshport: Column<Int> = integer("sshport")
  val sshpassword: Column<String> = varchar("sshpassword", 20)
  val ipv6: Column<String> = varchar("ipv6", 128)
  val dns_enabled: Column<Boolean> = bool("dns_enabled")
  val billing: Column<Boolean> = bool("billing")
}

object IdcConfigTable: Table("idc_config") {
  val idc: Column<String> = varchar("idc", 64)
  val serviceName: Column<String> = varchar("service_name", 128)
  val config: Column<String> = text("config")
  val version: Column<Int> = integer("version")
}

data class IdcConfig(val idc: String, val serviceName: String, val config: String, val version: Int)

fun <T: Table> T.insertQuery(body: T.(InsertStatement<Number>)->Unit): InsertStatement<Number>
    = InsertStatement<Number>(this).apply { body(this) }

fun <T: Table> T.updateQuery(where: SqlExpressionBuilder.()->Op<Boolean>, limit: Int? = null, body: T.
(UpdateStatement) -> Unit): UpdateStatement {
  val query = UpdateStatement(this, limit, SqlExpressionBuilder.where())
  body(query)
  return query
}

fun <T: Any> toPlainSQL(statement: Statement<T>, trans: Transaction): List<String> {
  val arguments = statement.arguments()
  val contexts = if (arguments.count() > 0) {
    arguments.map { args ->
      val context = StatementContext(statement, args)
      context
    }
  } else {
    val context = StatementContext(statement, emptyList())
    listOf(context)
  }

  return contexts.map { context ->
    context.expandArgs(trans)
  }
}

val agoraResource = Database.connect(
    "jdbc:mysql://${getGlobalSettings().resource.ip}:${getGlobalSettings().resource.port}/${getGlobalSettings().resource.dbName}",
    driver = "com.mysql.jdbc.Driver", user = getGlobalSettings().resource.userName,
    password = getGlobalSettings().resource.password)

