package org.khala.resource

import org.jetbrains.exposed.sql.*
import org.joda.time.DateTime
import org.khala.getGlobalSettings


object VendorInfoTable: Table("vendor_info") {
  val vendorId: Column<Int> = integer("vendor_id")
  val name: Column<String> = varchar("name", 50)
  val description: Column<String> = varchar("description", 11)
  val user: Column<String> = varchar("user", 50)
  val phone: Column<String> = varchar("phone", 20)
  val email: Column<String> = varchar("email", 256)
  val key: Column<String> = varchar("key", 32)
  val signkey: Column<String> = varchar("signkey", 32)
  val signkeySignal: Column<String> = varchar("signkey_signal", 32)
  val apiSecret: Column<String> = varchar("api_secret", 32)
  val status: Column<Int> = integer("status")
  val parentId: Column<Int> = integer("parent_id")
  val maxChannels: Column<Int> = integer("max_channels")
  val startTime: Column<DateTime> = datetime("start_time")
  val endTime: Column<DateTime> = datetime("end_time")
  val country: Column<String> = varchar("country", 256)
  val companyId: Column<Int> = integer("company_id")
  val projectId: Column<String> = varchar("project_id", 64)
  val inChannelPermission: Column<Int> = integer("in_channel_permission")
}

val agoraVendors = Database.connect(
    "jdbc:mysql://${getGlobalSettings().vendors.ip}:${getGlobalSettings().vendors.port}/${getGlobalSettings().vendors.dbName}",
    driver = "com.mysql.jdbc.Driver", user = getGlobalSettings().vendors.userName,
    password = getGlobalSettings().vendors.password)
