package org.khala.features.miniapp

import com.andreapivetta.kolor.green
import com.andreapivetta.kolor.red
import me.tongfei.progressbar.ProgressBar
import me.tongfei.progressbar.ProgressBarBuilder
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction
import org.khala.resource.*


internal fun findVendorById(vendorId: Int) = transaction(agoraVendors) {
  VendorInfoTable.select {
    VendorInfoTable.vendorId eq  vendorId
  }.firstOrNull()
}

internal fun findVendorByVendorKey(vendorKey: String) = transaction(agoraVendors) {
  VendorInfoTable.select {
    VendorInfoTable.key eq vendorKey
  }.firstOrNull()
}

internal fun isVendorCurrentlySupported(vendorId: Int): Boolean {
  return transaction(miniappDatabase) {
    VendorsTable.select {
      VendorsTable.vendorId eq vendorId
    }.firstOrNull()
  } != null
}

internal fun doAddSupportedVendor(vendorId: Int, vendorKey: String, verbose: Boolean, pb: ProgressBar): List<String> {
  if (isVendorCurrentlySupported(vendorId)) {
    pb.step()
    return listOf("Vendor ($vendorId, $vendorKey) exists.".green())
  }

  var rst = listOf<String>()

  transaction(miniappDatabase) {
    if (verbose) {
      val stmt = VendorsTable.insertQuery {
        it[VendorsTable.vendorId] = vendorId
        it[VendorsTable.key] = vendorKey
        it[VendorsTable.maxSubscribeLoad] = 100
        it[VendorsTable.maxResolution] = "300*300"
        it[VendorsTable.status] = 1
        it[VendorsTable.region] = 0
      }

      toPlainSQL(stmt, this).forEach {
        rst += "____execute sql statement for new vendor ($vendorId, $vendorKey):".green()
        rst += it
      }
    }

    VendorsTable.insert {
      it[VendorsTable.vendorId] = vendorId
      it[VendorsTable.key] = vendorKey
      it[VendorsTable.maxSubscribeLoad] = 100
      it[VendorsTable.maxResolution] = "300*300"
      it[VendorsTable.status] = 1
      it[VendorsTable.region] = 0
    }
    pb.step()
  }

  return rst
}

fun addSupportedVendor(vendorId: Int, verbose: Boolean): Pair<Boolean, List<String>> {
  val pb = ProgressBarBuilder().setInitialMax(1).setTaskName("ADD VENDOR").build()
  pb.use {
    findVendorById(vendorId).let { rr ->
      pb.step()
      if (rr == null) {
        return Pair(false, listOf("Invalid vendor id: $vendorId".red()))
      }
      val vendorKey = rr[VendorInfoTable.key]

      pb.maxHint(pb.max + 1)

      return Pair(true, doAddSupportedVendor(vendorId, vendorKey, verbose, pb))
    }
  }
}

fun addSupportedVendor(vendorKey: String, verbose: Boolean): Pair<Boolean, List<String>> {
  val pb = ProgressBarBuilder().setInitialMax(1).setTaskName("ADD VENDOR").build()
  pb.use {
    findVendorByVendorKey(vendorKey).let { rr ->
      pb.step()
      if (rr == null) {
        return Pair(false, listOf("Invalid vendor key: $vendorKey".red()))
      }
      val vendorId = rr[VendorInfoTable.vendorId]

      pb.maxHint(pb.max + 1)

      return Pair(true, doAddSupportedVendor(vendorId, vendorKey, verbose, pb))
    }
  }
}
