package org.khala.features.ap

import com.google.gson.JsonParser
import com.moandjiezana.toml.Toml
import org.funktionale.memoization.memoize
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.khala.Server
import org.khala.ServerIp
import org.khala.makeServer
import org.khala.resource.IdcConfig
import org.khala.resource.IdcConfigTable
import org.khala.resource.ServerTable
import org.khala.resource.agoraResource
import java.io.File
import java.nio.file.Paths

var filterAvailableServers: () -> List<Server> = {
  transaction(agoraResource) {
    ServerTable.selectAll().filter {
      it[ServerTable.down_time].isAfterNow
    }.map {
      makeServer(it)
    }.toList()
  }
}.memoize()

var availableServiceMap: () -> Map<String, Server> = {
  val rst = mutableMapOf<String, Server>()
  filterAvailableServers().map { svr ->
    svr.detailIp.map {
      rst.put(it.ip, svr)
    }
  }

  rst
}.memoize()

var filterAccessPointServers: () -> List<Server> = {
  transaction(agoraResource) {
    ServerTable.select { ServerTable.access_point eq true }.filter {
      it[ServerTable.down_time].isAfterNow
    }.map {
      makeServer(it)
    }.toList()
  }
}.memoize()

fun filterAccessPointIdc(): Set<String> {
  return filterAccessPointServers().map {
    it.idc
  }.toSet()
}

fun fetchIdcConfig(idc: String, serviceName: String): IdcConfig? = transaction(agoraResource) {
  IdcConfigTable.select {
    IdcConfigTable.idc.eq(idc) and IdcConfigTable.serviceName.eq(serviceName)
  }.map {
    IdcConfig(it[IdcConfigTable.idc], it[IdcConfigTable.serviceName],
        it[IdcConfigTable.config], it[IdcConfigTable.version])
  }.firstOrNull()
}


fun parseConfigBody(json: String): List<BackendServiceBinding> {
  return JsonParser().parse(json).asJsonArray.map {
    BackendServiceBinding.fromJson(it)
  }
}

fun eqBackendServicesBindingList(s1: List<BackendServiceBinding>, s2: List<BackendServiceBinding>): Boolean {
  val map1 = s1.map {
    it.address to it.tags
  }.toMap()
  val map2 = s2.map {
    it.address to it.tags
  }.toMap()

  if (map1.keys != map2.keys) return false

  return map1.keys.all {
    map1[it] == map2[it]
  }
}

fun checkBackendServiceListDifferences(s1: List<BackendServiceBinding>,
                                       s2: List<BackendServiceBinding>): ServiceListDifference {
  val map1 = s1.map {
    it.address to it.tags
  }.toMap()
  val map2 = s2.map {
    it.address to it.tags
  }.toMap()

  val removeSet = map1.mapNotNull filter@{
    if (!map2.containsKey(it.key)) {
      return@filter BackendServiceBinding(it.key, it.value)
    }
    null
  }

  val newSet = map2.mapNotNull filter@{
    if (!map1.containsKey(it.key)) {
      return@filter BackendServiceBinding(it.key, it.value)
    }

    null
  }

  val modifySet = map1.mapNotNull {
    if (map2.containsKey(it.key)) {
      if (map2[it.key] != it.value) {
        return@mapNotNull Pair(BackendServiceBinding(it.key, it.value), BackendServiceBinding(it.key, map2[it.key]!!))
      }
    }

    null
  }

  return ServiceListDifference(newSet, removeSet, modifySet)
}

fun readBackendServiceConfigs(name: String): BackendService {
  val path = Paths.get(System.getenv("HOME"), "OneDrive/agora/ap_resource_info", "services", "$name.toml")
  return Toml().read(File(path.toString())).to(BackendService::class.java)
}

fun readBackendServiceToServer(name: String): List<Server> {
  val cfg = readBackendServiceConfigs(name)
  val allServers = filterAvailableServers()

  return cfg.servers.mapNotNull {
    if (it.address == "127.0.0.1") {
      Server("", listOf(ServerIp("NA", "127.0.0.1")), "")
    } else {
      allServers.firstOrNull { server ->
        server.findISP(it.address) != null
      }
    }
  }.toList()
}

fun findServerByIp(ip: String): Server? = availableServiceMap()[ip]

