package org.khala

import com.moandjiezana.toml.Toml
import org.funktionale.memoization.memoize
import java.io.File
import java.nio.file.Paths


data class ResourceSettings(val ip: String, val port: Int, val dbName: String,
                            val userName: String, val password: String)

data class Settings(val resource: ResourceSettings,
                    val vendors: ResourceSettings,
                    val miniapp: ResourceSettings)


val getGlobalSettings: () -> Settings = {
  val path = Paths.get(System.getenv("HOME"), "OneDrive/agora/ap_resource_info", "settings.toml")
  Toml().read(File(path.toString())).to(Settings::class.java)
}.memoize()
