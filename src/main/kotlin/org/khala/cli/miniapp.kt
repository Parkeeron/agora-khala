package org.khala.cli

import com.andreapivetta.kolor.green
import com.andreapivetta.kolor.red
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.types.int
import org.khala.features.miniapp.addSupportedVendor

class Miniapp: CliktCommand(name = "miniapp", help = "Miniapp Supported Vendors Generation") {
  private val verbose by option("-v", "--verbose",
      help = "Enable low-level detail messages.").flag(default = false)
  private val vendorId by option("--vendor-id", help = "Id of vendor to be supported.").int()
  private val vendorKey by option("--vendor-key", help = "Key of vendor to be supported.")

  override fun run() {
    if (vendorId != null && vendorKey != null) {
      println("Can't use vendor_id and vendor_key at the same time.".red())
      return
    }

    if (vendorId == null && vendorKey == null) {
      println("Must specify vendor_id or vendor_key.".red())
      return
    }

    var res: Pair<Boolean, List<String>?> = Pair(false, null)
    vendorId?.let {
      res = addSupportedVendor(it, verbose)
    }
    vendorKey?.let {
      res = addSupportedVendor(it, verbose)
    }

    println()
    res.second?.let { it ->
      it.forEach {
        println(it)
      }
    }

    if (res.first) {
      println("Add new supported vendor success.".green())
    } else {
      println("Add new supported vendor failed.".red())
    }
  }
}


